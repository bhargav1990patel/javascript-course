/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var globalScore, roundScore, player, isGameActive, winningScore;

function newGame(){
    document.getElementById('dice-1').style.display = 'none';
    document.getElementById('dice-2').style.display = 'none';
    document.querySelector('#current-0').textContent = 0;
    document.querySelector('#current-1').textContent = 0;
    document.querySelector('#score-0').textContent = 0;
    document.querySelector('#score-1').textContent = 0;
    globalScore = [0,0];
    roundScore = 0;
    activePlayer = 0;
    isGameActive = true;
    winningScore = 100; //window.prompt('Please type the winning score, i.e. 50, 100, 200');

    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.querySelector('.final-score').value = '';

}

newGame();

document.querySelector('.btn-new').addEventListener('click',newGame);

document.querySelector('.btn-roll').addEventListener('click',function(){
    if(isGameActive){
        // 1. Get the random dice number
        var dice1 = Math.floor( (Math.random() * 6) + 1) ;
        var dice2 = Math.floor( (Math.random() * 6) + 1) ;

        // 2. Show the new dice on screen
        document.getElementById('dice-1').style.display = 'block';
        document.getElementById('dice-1').src = 'dice-'+dice1+'.png';
        document.getElementById('dice-2').style.display = 'block';
        document.getElementById('dice-2').src = 'dice-'+dice2+'.png';

        // 3. Update the current round score if Rolled dice <> 1 then add else 0
        if(dice1 !== 1 && dice2 !== 1) {
            roundScore += dice1 + dice2;
        }  else
         roundScore = 0;

        document.querySelector('#current-'+activePlayer).textContent = roundScore;

        // if score became 0, change the player
        if(roundScore === 0) nextPlayer();    
    }
    
})

document.querySelector('.btn-hold').addEventListener('click',function(){
    if(isGameActive){
        // update the global score for current player
        globalScore[activePlayer] += roundScore;
        document.querySelector('#score-'+activePlayer).textContent = globalScore[activePlayer];

        var input = document.querySelector('.final-score').value;

        if(input) winningScore = input;

        if(globalScore[activePlayer] >= winningScore){
            document.getElementById('name-'+activePlayer).textContent = 'Winner!';
            document.getElementById('dice-1').style.display = 'none';
            document.getElementById('dice-2').style.display = 'none';
            document.querySelector('.player-'+activePlayer+'-panel').classList.add('winner');
            document.querySelector('.player-'+activePlayer+'-panel').classList.remove('active');
            isGameActive = false;
        }
        else nextPlayer();    // change the player
    }
    
})

function nextPlayer(){
    if(roundScore !== 0) {
        document.getElementById('dice-1').style.display = 'none';
        document.getElementById('dice-2').style.display = 'none';
    }
    
    // when next player starts, set all scores to 0
    roundScore = 0; 
    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;
    
    // change the currnet player
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;

    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
}